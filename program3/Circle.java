/**
 * Circle Class
 *
 * @author Christopher Patton
 * @version Program 3
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Color;
import java.awt.Point;

public class Circle implements Shape
{
   private double r;
   private Point p;
   private Color clr;
   private boolean f;

   public Circle(double radius, java.awt.Point position,
                 java.awt.Color color, boolean filled)
   {
      this.r = radius;
      this.p = position;
      this.clr = color;
      this.f = filled;
   }

   public double getRadius()
   {
      return this.r;
   }

   public void setRadius(double radius)
   {
      this.r = radius;
   }

   public double getArea()
   {
      return Math.PI * this.r * this.r;
   }

   public java.awt.Color getColor()
   {
      return this.clr;
   }

   public void setColor(java.awt.Color color)
   {
      this.clr = color;
   }

   public boolean getFilled()
   {
      return this.f;
   }

   public void setFilled(boolean filled)
   {
      this.f = filled;
   }

   public java.awt.Point getPosition()
   {
      return this.p;
   }

   public void setPosition(java.awt.Point position)
   {
      this.p.setLocation(position);
   }

   public void move(java.awt.Point delta)
   {
      this.p.setLocation(this.p.getX() + delta.getX(),
                         this.p.getY() + delta.getY());
   }

   public boolean equals(Object other)
   {
      return (other != null &&
              other.getClass() == this.getClass() &&
              this.f == ((Circle)other).f &&
              this.clr.equals(((Circle)other).clr) &&
              this.p.equals(((Circle)other).p) &&
              this.r == ((Circle)other).r);
   }
}
