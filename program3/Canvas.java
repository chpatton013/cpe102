/**
 * Canvas Class for Shapes
 *
 * @author Christopher Patton
 * @version Program 3
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Point;
import java.awt.Color;

public class Canvas
{
   private java.util.ArrayList<Shape> s = new java.util.ArrayList<Shape>();

   public Canvas()
   {
   }

   public void add(Shape shape)
   {
      this.s.add(shape);
   }

   public Shape remove(int index)
   {
      return this.s.remove(index);
   }

   public Shape get(int index)
   {
      return this.s.get(index);
   }

   public int size()
   {
      return this.s.size();
   }

   public java.util.ArrayList<Circle> getCircles()
   {
      java.util.ArrayList<Circle> list = new java.util.ArrayList<Circle>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getClass().getName().equals("Circle"))
         {
            list.add((Circle)this.s.get(i));
         }
      }

      return list;
   }

   public java.util.ArrayList<Rectangle> getRectangles()
   {
      java.util.ArrayList<Rectangle> list =
         new java.util.ArrayList<Rectangle>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getClass().getName().equals("Rectangle"))
         {
            list.add((Rectangle)this.s.get(i));
         }
      }

      return list;
   }

   public java.util.ArrayList<Triangle> getTriangles()
   {
      java.util.ArrayList<Triangle> list = new java.util.ArrayList<Triangle>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getClass().getName().equals("Triangle"))
         {
            list.add((Triangle)this.s.get(i));
         }
      }

      return list;
   }

   public java.util.ArrayList<ConvexPolygon> getConvexPolygons()
   {
      java.util.ArrayList<ConvexPolygon> list =
         new java.util.ArrayList<ConvexPolygon>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getClass().getName().equals("ConvexPolygon"))
         {
            list.add((ConvexPolygon)this.s.get(i));
         }
      }

      return list;
   }

   public java.util.ArrayList<Shape> getShapesByColor(java.awt.Color color)
   {
      java.util.ArrayList<Shape> list = new java.util.ArrayList<Shape>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getColor().equals(color))
         {
            list.add(this.s.get(i));
         }
      }

      return list;
   }

   public double getAreaOfAllShapes()
   {
      double a = 0.0;

      for (int i = 0; i < this.s.size(); i++)
      {
         a += this.s.get(i).getArea();
      }

      return a;
   }
}
