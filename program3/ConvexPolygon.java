/**
 * Convex Polygon Class
 *
 * @author Christopher Patton
 * @version Program 3
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Color;
import java.awt.Point;

public class ConvexPolygon implements Shape
{
   private Point[] v;
   private Color clr;
   private boolean f;

   public ConvexPolygon(java.awt.Point[] vertices, java.awt.Color color,
                        boolean filled)
   {
      this.v = new Point[vertices.length];

      for (int i = 0; i < vertices.length; i++)
      {
         this.v[i] = vertices[i];
      }

      this.clr = color;
      this.f = filled;
   }

   public java.awt.Point getVertex(int index)
   {
      return this.v[index];
   }

   public void setVertex(int index, java.awt.Point vertex)
   {
      this.v[index].setLocation(vertex);
   }

   public double getArea()
   {
      int i = 0;
      double a = 0.0;

      for (i = 0; i < this.v.length - 1; i++)
      {
         a += (this.v[i].getX() * this.v[i + 1].getY() -
               this.v[i].getY() * this.v[i + 1].getX());
      }

      a += (this.v[i].getX() * this.v[0].getY() -
            this.v[i].getY() * this.v[0].getX());

      return a / 2.0;
   }

   public java.awt.Color getColor()
   {
      return this.clr;
   }

   public void setColor(java.awt.Color color)
   {
      this.clr = color;
   }

   public boolean getFilled()
   {
      return this.f;
   }

   public void setFilled(boolean filled)
   {
      this.f = filled;
   }

   public java.awt.Point getPosition()
   {
      return this.v[0];
   }

   public void setPosition(java.awt.Point position)
   {
      int xOff = (int)(position.getX() - this.v[0].getX());
      int yOff = (int)(position.getY() - this.v[0].getY());

      for (int i = 0; i < this.v.length; i++)
      {
         this.v[i] = new Point((int)(this.v[i].getX() + xOff),
                               (int)(this.v[i].getY() + yOff));
      }
   }

   public void move(java.awt.Point delta)
   {
      for (int i = 0; i < this.v.length; i++)
      {
         this.v[i].setLocation(this.v[i].getX() + delta.getX(),
                               this.v[i].getY() + delta.getY());
      }
   }

   public boolean equals(Object other)
   {
      if (other == null ||
          other.getClass() != this.getClass() ||
          this.f != ((ConvexPolygon)other).f ||
          !this.clr.equals(((ConvexPolygon)other).clr) ||
          this.v.length != ((ConvexPolygon)other).v.length)
      {
         return false;
      }

      for (int i = 0; i < this.v.length; i++)
      {
         if (!this.v[i].equals(((ConvexPolygon)other).v[i]))
         {
            return false;
         }
      }

      return true;
   }
}
