/**
 * Triangle Class
 *
 * @author Christopher Patton
 * @version Program 3
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Color;
import java.awt.Point;

public class Triangle implements Shape
{
   private Point a, b, c;
   private Color clr;
   private boolean f;

   public Triangle(java.awt.Point a, java.awt.Point b, java.awt.Point c,
                   java.awt.Color color, boolean filled)
   {
      this.a = a;
      this.b = b;
      this.c = c;
      this.clr = color;
      this.f = filled;
   }

   public java.awt.Point getVertexA()
   {
      return this.a;
   }

   public void setVertexA(java.awt.Point point)
   {
      this.a.setLocation(point);
   }

   public java.awt.Point getVertexB()
   {
      return this.b;
   }

   public void setVertexB(java.awt.Point point)
   {
      this.b.setLocation(point);
   }

   public java.awt.Point getVertexC()
   {
      return this.c;
   }

   public void setVertexC(java.awt.Point point)
   {
      this.c.setLocation(point);
   }

   public double getArea()
   {
      double ab = distance(this.a, this.b);
      double bc = distance(this.b, this.c);
      double ca = distance(this.c, this.a);
      double s = (ab + bc + ca) / 2.0;

      return Math.sqrt(s * (s - ab) * (s - bc) * (s - ca));
   }

   public java.awt.Color getColor()
   {
      return this.clr;
   }

   public void setColor(java.awt.Color color)
   {
      this.clr = color;
   }

   public boolean getFilled()
   {
      return this.f;
   }

   public void setFilled(boolean filled)
   {
      this.f = filled;
   }

   public java.awt.Point getPosition()
   {
      return this.a;
   }

   public void setPosition(java.awt.Point position)
   {
      int xOff = (int)(position.getX() - this.a.getX());
      int yOff = (int)(position.getY() - this.a.getY());

      this.a = new Point((int)(this.a.getX() + xOff),
                         (int)(this.a.getY() + yOff));
      this.b = new Point((int)(this.b.getX() + xOff),
                         (int)(this.b.getY() + yOff));
      this.c = new Point((int)(this.c.getX() + xOff),
                         (int)(this.c.getY() + yOff));
   }

   public void move(java.awt.Point delta)
   {
      this.a.setLocation(this.a.getX() + delta.getX(),
                         this.a.getY() + delta.getY());
      this.b.setLocation(this.b.getX() + delta.getX(),
                         this.b.getY() + delta.getY());
      this.c.setLocation(this.c.getX() + delta.getX(),
                         this.c.getY() + delta.getY());
   }

   public boolean equals(Object other)
   {
      return (other != null &&
              other.getClass() == this.getClass() &&
              this.f == ((Triangle)other).f &&
              this.clr.equals(((Triangle)other).clr) &&
              this.a.equals(((Triangle)other).a) &&
              this.b.equals(((Triangle)other).b) &&
              this.c.equals(((Triangle)other).c));
   }

   private double distance(java.awt.Point p1, java.awt.Point p2)
   {
      return Math.sqrt((p2.getX() - p1.getX()) * (p2.getX() - p1.getX()) +
                       (p2.getY() - p1.getY()) * (p2.getY() - p1.getY()));
   }
}
