/**
 * This interface is implemented by the Circle, Triangle, Rectangle, and
 * ConvexPolygon classes.
 *
 * @author Christopher Patton
 * @version Program 3
 * @version CPE102-09
 * @version Spring 2011
 */

public interface Shape
{
   /**
    * This method calculates and returns the area contained by a Shape.
    *
    * @return Returns a double approximation of the area of a Shape.
    */
   public double getArea();

   /**
    * This method returns the Color of a Shape.
    *
    * @return Returns a reference to the java.awt.Color of a Shape.
    */
   public java.awt.Color getColor();

   /**
    * This method sets the Color of a Shape.
    *
    * @param color Takes a reference to a java.awt.Color.
    */
   public void setColor(java.awt.Color color);

   /**
    * This method returns the filled state of a Shape.
    *
    * @return Returns a boolean describing the filled state of a Shape.
    */
   public boolean getFilled();

   /**
    * This method sets the filled state of a Shape.
    *
    * @param filled Takes a boolean to describe the filled state of a Shape.
    */
   public void setFilled(boolean filled);

   /**
    * This method returns location of a Shape.
    *
    * @return Returns a reference to the java.awt.Point location of a Shape.
    */
   public java.awt.Point getPosition();

   /**
    * This method sets the location of a Shape.
    *
    * @param position Takes a reference to the java.awt.Point location of a Shape.
    */
   public void setPosition(java.awt.Point position);

   /**
    * This method changes the location of a Shape by some offset Point.
    *
    * @param delta Takes a reference to the java.awt.Point offset of a Shape.
    */
   public void move(java.awt.Point delta);
}
