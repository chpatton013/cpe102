/**
 * Rectangle Class
 *
 * @author Christopher Patton
 * @version Program 3
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Color;
import java.awt.Point;

public class Rectangle implements Shape
{
   private int w, h;
   private Point p;
   private Color clr;
   private boolean f;

   public Rectangle(int width, int height, java.awt.Point position,
                    java.awt.Color color, boolean filled)
   {
      this.w = width;
      this.h = height;
      this.p = position;
      this.clr = color;
      this.f = filled;
   }

   public int getWidth()
   {
      return this.w;
   }

   public void setWidth(int width)
   {
      this.w = width;
   }

   public int getHeight()
   {
      return this.h;
   }

   public void setHeight(int height)
   {
      this.h = height;
   }

   public double getArea()
   {
      return this.w * this.h;
   }

   public java.awt.Color getColor()
   {
      return this.clr;
   }

   public void setColor(java.awt.Color color)
   {
      this.clr = color;
   }

   public boolean getFilled()
   {
      return this.f;
   }

   public void setFilled(boolean filled)
   {
      this.f = filled;
   }

   public java.awt.Point getPosition()
   {
      return this.p;
   }

   public void setPosition(java.awt.Point position)
   {
      this.p.setLocation(position);
   }

   public void move(java.awt.Point delta)
   {
      this.p.setLocation(this.p.getX() + delta.getX(),
                         this.p.getY() + delta.getY());
   }

   public boolean equals(Object other)
   {
      return (other != null &&
              other.getClass() == this.getClass() &&
              this.f == ((Rectangle)other).f &&
              this.clr.equals(((Rectangle)other).clr) &&
              this.p.equals(((Rectangle)other).p) &&
              this.w == ((Rectangle)other).w &&
              this.h == ((Rectangle)other).h);
   }
}
