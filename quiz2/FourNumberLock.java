/**
 * @author Christopher Patton
 * @version Lab Quiz 2
 */

public class FourNumberLock extends ThreeNumberLock
{
   private int n4;

   public FourNumberLock(int num1, int num2, int num3, int num4)
   {
      super(num1, num2, num3);

      if (num4 < 0 || num4 > 99)
      {
         throw new IllegalArgumentException();
      }

      this.n4 = num4;
   }

   public boolean open(int num1, int num2, int num3, int num4)
   {
      if (super.open(num1, num2, num3) && this.n4 == num4)
      {
         return true;
      }

      super.close();
      return false;
   }

   public boolean equals(Object other)
   {
      return (super.equals(other) &&
              this.n4 == ((FourNumberLock)other).n4);
   }

   public int compareTo(ThreeNumberLock other)
   {
      return 1;
   }

   public int compareTo(FourNumberLock other)
   {
      int comp = super.compareTo((ThreeNumberLock)other);

      if (comp != 0)
      {
         return comp;
      }

      else if (this.n4 > other.n4)
      {
         return 1;
      }

      else if (this.n4 < other.n4)
      {
         return -1;
      }

      return 0;
   }
}