/**
 * @author Christopher Patton
 * @version Lab Quiz 2
 */

public class ThreeNumberLock implements Comparable<ThreeNumberLock>
{
   private int n1, n2, n3;
   private boolean lock = false;

   public ThreeNumberLock(int num1, int num2, int num3)
   {
      if (num1 < 0 || num1 > 99 || num2 < 0 || num2 > 99 ||
          num3 < 0 || num3 > 99)
      {
         throw new IllegalArgumentException();
      }

      this.n1 = num1;
      this.n2 = num2;
      this.n3 = num3;
   }

   public boolean open(int num1, int num2, int num3)
   {
      if (this.n1 == num1 && this.n2 == num2 && this.n3 == num3)
      {
         this.lock = true;
         return true;
      }

      return false;
   }

   public boolean isOpen()
   {
      return this.lock;
   }

   public void close()
   {
      this.lock = false;
   }

   public boolean equals(Object other)
   {
      return (other != null &&
              this.getClass() == other.getClass() &&
              this.n1 == ((ThreeNumberLock)other).n1 &&
              this.n2 == ((ThreeNumberLock)other).n2 &&
              this.n3 == ((ThreeNumberLock)other).n3 &&
              this.lock == ((ThreeNumberLock)other).lock);
   }

   public int compareTo(ThreeNumberLock other)
   {
      if (this.n1 > other.n1)
      {
         return 1;
      }

      else if (this.n1 < other.n1)
      {
         return -1;
      }

      else if (this.n2 > other.n2)
      {
         return 1;
      }

      else if (this.n2 < other.n2)
      {
         return -1;
      }

      else if (this.n3 > other.n3)
      {
         return 1;
      }

      else if (this.n3 < other.n3)
      {
         return -1;
      }

      return 0;
   }
}