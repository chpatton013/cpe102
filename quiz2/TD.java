public class TD
{
   public static void main(String[] args)
   {

      FourNumberLock l1 = new FourNumberLock(11, 22, 33, 44);
      FourNumberLock l2 = new FourNumberLock(11, 22, 33, 44);
      FourNumberLock l3 = new FourNumberLock(44, 33, 22, 11);

      l1.open(11, 22, 33, 44);

      if (!l1.isOpen()) System.out.println("fail; l1.isOpen");
      if (l2.isOpen()) System.out.println("fail; l2.isOpen");

      if (l1.equals(l2)) System.out.println("fail; l1.equals(l2)");
      l1.close();
      if (!l1.equals(l2)) System.out.println("fail; l1.equals(l2)");

      if (l1.compareTo(l2) != 0)
         System.out.println("fail; l1.compareTo(l2)");
      if (l1.compareTo(l3) != -1)
         System.out.println("fail; l1.compareTo(l3)");
      if (l3.compareTo(l1) != 1)
         System.out.println("fail; l3.compareTo(l1)");



   }
}