/**
 * Dictionary Class
 *
 * @author Christopher Patton
 * @version Program 5
 */

import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

public class Dictionary implements Iterable<String>
{
   private ArrayList<String> d = new ArrayList<String>();

   /**
    * Constructs a dictionary from the specified text file that contains
    * one word per line and, if necessary, sorts it.
    *
    * @param fileName The file to read the words from (one word per 
    *                 line).
    * @param sort Indicates if the words needs sorting, true if the 
    *             input file needs sorting, otherwise false.
    *
    * @throws DictionaryExpection when, for any reason, the Dictionary
    *                             could not be created.
    */
   public Dictionary(String fileName, boolean sort)
    throws DictionaryException
   {
      FileInputStream f;

      try
      {
         f = new FileInputStream(new File(fileName));
      }
      catch(Exception e)
      {
         throw new DictionaryException();
      }

      Scanner in = new Scanner(f);

      try
      {
         while (true)
         {
            d.add(in.next());
         }
      }
      catch(java.util.NoSuchElementException e)
      {
      }

      d.trimToSize();

      if (sort)
      {
         quicksort(0, d.size() - 1);
      }

      try
      {
         in.close();
         f.close();
      }
      catch(Exception e)
      {
         throw new DictionaryException();
      }
   }

   /**
    * Provides a java.util.Iterator that iterates over the Dictionary in
    * order, a word at a time, from beginning to end.
    *
    * @return A java.util.Iterator reference that iterates over the
    *         Dictionary a word at a time.
    */
   public Iterator<String> iterator()
   {
      return d.iterator();
   }

   /**
    * Looks up the specified word in the Dictionary using a Binary
    * Search algorithm that you write (Do not use any search methods
    * from the Java Standard Library). 
    *
    * @param word String to be looked up.
    *
    * @return true if the word is in the Dictionary, otherwise false.
    */
   public boolean lookUp(String word)
   {
      int low = 0, high = d.size() - 1, mid, result;

      while (low <= high)
      {
         mid = low + (high - low) / 2;
         result = word.compareTo(d.get(mid));

         if (result > 0)
         {
            low = mid + 1;
         }

         else if (result < 0)
         {
            high = mid - 1;
         }

         else
         {
            return true;
         }
      }

      return false;
   }

   /**
    * Writes the Dictionary to a text file using the java.io.PrintStream
    * class to the specified name in ascending order and with a newline
    * character between each word. 
    *
    * @param fileName The name of the file to write to.
    *
    * @throws DictionaryException 
    */
   public void write(String fileName) throws DictionaryException
   {
      PrintStream out;

      try
      {
         out = new PrintStream(new File(fileName));
      }

      catch(Exception e)
      {
         throw new DictionaryException();
      }

      for (String s : d)
      {
         out.println(s);
      }

      out.close();
   }

   private void quicksort(int l, int r)
   {
      int i = l;
      int j = r;

      String p = new String(d.get(l + (r - l) / 2));

      while (i <= j)
      {
         while (d.get(i).compareTo(p) < 0)
         {
            i++;
         }

         while (d.get(j).compareTo(p) > 0)
         {
            j--;
         }

         if (i <= j)
         {
            swap(i, j);
            i++;
            j--;
         }
      }

      if (l < j)
      {
         quicksort(l, j);
      }

      if (i < r)
      {
         quicksort(i, r);
      }
   }

   private void swap(int s1, int s2)
   {
      String temp = new String(d.get(s1));
      d.set(s1, d.get(s2));
      d.set(s2, temp);
   }
}
