/**
 * DictionaryException Class
 *
 * @author Christopher Patton
 * @version Program5
 */

public class DictionaryException extends Exception
{
   public DictionaryException()
   {
   }

   public DictionaryException(String message)
   {
      super(message);
   }
}
