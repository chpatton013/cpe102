public class TD
{
   public static void main(String[] args) throws DictionaryException
   {
      Dictionary s = new Dictionary("fullOrderedDict.txt", true);
      Dictionary u = new Dictionary("partialRandomDict.txt", false);

      System.out.println("sorted");
      System.out.println(s.lookUp("GENERIC"));
      System.out.println(s.lookUp("QWERTYQ"));
      s.write("sortedOutput.txt");

      System.out.println("unsorted");
      System.out.println(u.lookUp("GENERIC"));
      System.out.println(u.lookUp("QWERTYQ"));
      u.write("unsortedOutput.txt");
   }
}
