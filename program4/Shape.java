/**
 * Abstract Shape class
 *
 * @author Christopher Patton
 * @version Program 4
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Color;
import java.awt.Point;

public abstract class Shape implements java.lang.Comparable<Shape>
{
   private boolean f;
   private Color c;

   /**
    * Shape Constructor.
    *
    * @param color Color of shape.
    * @param filled Filled state of shape.
    */
   public Shape (Color color, boolean filled)
   {
      this.f = filled;
      this.c = color;
   }

   /**
    * Calculates and returns area contained in shape.
    *
    * @return Area of shape.
    */
   public abstract double getArea();

   /**
    * Returns color of shape.
    *
    * @return Color of shape.
    */
   public Color getColor()
   {
      return this.c;
   }

   /**
    * Sets the color of the shape.
    *
    * @param color Color to set shape color to.
    */
   public void setColor(Color color)
   {
      this.c = color;
   }

   /**
    * Returns filled state of shape.
    *
    * @return Filled state of shape.
    */
   public boolean getFilled()
   {
      return this.f;
   }

   /**
    * Sets filled state of shape.
    *
    * @param filled Filled state to set shape filled state to.
    */
   public void setFilled(boolean filled)
   {
      this.f = filled;
   }

   /**
    * Returns position of first vertex of shape.
    *
    * @return Position of first vertex of shape. 
    */
   public abstract Point getPosition();

   /**
    * Sets position of shape by moving all vertices in relation
    * to eachother.
    *
    * @param position Position to move first vertex to.
    */
   public abstract void setPosition(Point position);

   /**
    * Moves shape by some Point delta.
    *
    * @param delta Point representing difference in x- and y-coordinates
    *              to move all vertices of shape by.
    */
   public abstract void move(Point delta);
 
   /**
    * Compares two Shapes for equality. First checks class name
    * alphabetically, then shape area.
    *
    * @param other Other shape to compare current to.
    * @return Returns: 0 if equal, 1 if greater than other,
    *                  -1 if less than other.
    */
   public int compareTo(Shape other)
   {
      if (this.getClass().getName().compareTo
         (other.getClass().getName()) > 0)
      {
         return 1;
      }

      else if (this.getClass().getName().compareTo
         (other.getClass().getName()) < 0)
      {
         return -1;
      }

      else if (this.getArea() > other.getArea())
      {
         return 1;
      }

      else if (this.getArea() < other.getArea())
      {
         return -1;
      }

      else
      {
         return 0;
      }
   }

   /**
    * Compares two Objects for equality.
    *
    * @param other Other object to compare to.
    * @return Returns false if passed null reference or object of
    *         conflicting class type.
    *         Returns true if all instance variables are equivalent. 
    */
   public boolean equals(Object other)
   {
      return (other != null &&
              this.getClass() == other.getClass() &&
              this.f == ((Shape)other).f &&
              this.c.equals(((Shape)other).c));
   }
}
