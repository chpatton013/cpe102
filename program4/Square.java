/**
 * Square class: extends Rectangle
 *
 * @author Christopher Patton
 * @version Program 4
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Point;
import java.awt.Color;

public class Square extends Rectangle
{

   /**
    * Square Constructor.
    *
    * @param size Length of side of sqare.
    * @param position Position of first vertex.
    * @param color Color of square.
    * @param filled Filled state of square.
    */
   public Square(int size, Point position, Color color, boolean filled)
   {
      super(size, size, position, color, filled);
   }

   /**
    *
    */
   public int getSize()
   {
      return getWidth();
   }

   /**
    * Sets size of sides of square.
    *
    * @param size New size of sides of square.
    */
   public void setSize(int size)
   {
      super.setHeight(size);
      super.setWidth(size);
   }

   /**
    * Sets height of square. Overrides Rectangle's setHeight to maintain 
    * equivalent height and width.
    *
    * @param size New size of sides of square.
    */
   public void setHeight(int size)
   {
      setSize(size);
   }

   /**
    * Sets width of square. Overrides Rectangle's setWidth to maintain
    * equivalent height and width.
    *
    * @param size New size of sides of square.
   */
   public void setWidth(int size)
   {
      setSize(size);
   }
}
