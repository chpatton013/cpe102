/**
 * Circle class: extends Shape
 *
 * @author Christopher Patton
 * @version Program 4
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Color;
import java.awt.Point;

public class Circle extends Shape
{
   private double r;
   private Point p;

   public Circle(double radius, Point position, Color color, boolean filled)
   {
      super(color, filled);
      this.r = radius;
      this.p = new Point(position);
   }

   public double getRadius()
   {
      return this.r;
   }

   public void setRadius(double radius)
   {
      this.r = radius;
   }

   public double getArea()
   {
      return Math.PI * this.r * this.r;
   }

   public Point getPosition()
   {
      return new Point(this.p);
   }

   public void setPosition(Point position)
   {
      this.p = new Point(position);
   }

   public void move(Point delta)
   {
      this.p.setLocation(this.p.x + delta.x, this.p.y + delta.y);
   }

   public boolean equals(Object other)
   {
      return (super.equals(other) &&
              this.r == ((Circle)other).r &&
              this.p.equals(((Circle)other).p));
   }
}
