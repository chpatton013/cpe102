/**
 * Canvas Class for Shapes
 *
 * @author Christopher Patton
 * @version Program 4
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Point;
import java.awt.Color;
import java.util.ArrayList;

public class Canvas
{
   private ArrayList<Shape> s = new ArrayList<Shape>();

   /**
    * Default Canvas Constructor.
    */
   public Canvas()
   {
   }

   /**
    * Adds shape to Canvas.
    *
    * @param shape New shape to add to Canvas.
    */
   public void add(Shape shape)
   {
      this.s.add(shape);
   }

   /**
    * Removes shape from Canvas.
    *
    * @param index Index of shape to remove from Canvas.
    * @return Returns reference to removed Shape.
    */
   public Shape remove(int index)
   {
      return this.s.remove(index);
   }

   /**
    * Returns reference to specified Shape.
    *
    * @param index Index of shape to return.
    * @return Returns reference to specified Shape.
    */
   public Shape get(int index)
   {
      return this.s.get(index);
   }

   /**
    * Returns number of Shapes in Canvas.
    *
    * @return Returns number of Shapes in Canvas.
    */
   public int size()
   {
      return this.s.size();
   }

   /**
    * Returns an ArrayList of all Circle Shapes in Canvas.
    *
    * @return Returns an ArrayList of all Circle Shapes in Canvas.
    */
   public ArrayList<Circle> getCircles()
   {
      ArrayList<Circle> list = new ArrayList<Circle>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getClass().getName().equals("Circle"))
         {
            list.add((Circle)this.s.get(i));
         }
      }

      return list;
   }

   /**
    * Returns an ArrayList of all ConvexPolygon Shapes in Canvas.
    *
    * @return Returns an ArrayList of all ConvexPolygon Shapes in 
    *         Canvas.
    */
   public ArrayList<ConvexPolygon> getConvexPolygons()
   {
      ArrayList<ConvexPolygon> list = new ArrayList<ConvexPolygon>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getClass().getName().equals("ConvexPolygon"))
         {
            list.add((ConvexPolygon)this.s.get(i));
         }
      }

      return list;
   }

   /**
    * Returns an ArrayList of all Triangle Shapes in Canvas.
    *
    * @return Returns an ArrayList of all Triangle Shapes in Canvas.
    */
   public ArrayList<Triangle> getTriangles()
   {
      ArrayList<Triangle> list = new ArrayList<Triangle>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getClass().getName().equals("Triangle"))
         {
            list.add((Triangle)this.s.get(i));
         }
      }

      return list;
   }

   /**
    * Returns an ArrayList of all Rectangle Shapes in Canvas.
    *
    * @return Returns an ArrayList of all Rectangle Shapes in Canvas.
    */
   public ArrayList<Rectangle> getRectangles()
   {
      ArrayList<Rectangle> list = new ArrayList<Rectangle>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getClass().getName().equals("Rectangle"))
         {
            list.add((Rectangle)this.s.get(i));
         }
      }

      return list;
   }

   /**
    * Returns an ArrayList of all Shapes of a specified color in Canvas.
    *
    * @return Returns an ArrayList of all Shapes of a specified color
    *         in Canvas.
    */
   public ArrayList<Shape> getShapesByColor(Color color)
   {
      ArrayList<Shape> list = new ArrayList<Shape>();

      for (int i = 0; i < this.s.size(); i++)
      {
         if (this.s.get(i).getColor().equals(color))
         {
            list.add(this.s.get(i));
         }
      }

      return list;
   }

   /**
    * Calculates and returns the area of all Shapes in Canvas.
    *
    * @return Returns combined area of all Shapes in Canvas.
    */
   public double getAreaOfAllShapes()
   {
      double a = 0.0;

      for (int i = 0; i < this.s.size(); i++)
      {
         a += this.s.get(i).getArea();
      }

      return a;
   }
}
