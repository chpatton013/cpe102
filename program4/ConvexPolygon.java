/**
 * Convex Polygon class: extends Shape
 *
 * @author Christopher Patton
 * @version Program 4
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Color;
import java.awt.Point;

public class ConvexPolygon extends Shape
{
   private Point[] v;

   public ConvexPolygon(Point[] vertices, Color color, boolean filled)
   {
      super(color, filled);

      this.v = new Point[vertices.length];

      for (int i = 0; i < vertices.length; i++)
      {
         this.v[i] = new Point(vertices[i]);
      }
   }

   public Point getVertex(int index)
   {
      return new Point(this.v[index]);
   }

   public void setVertex(int index, Point vertex)
   {
      this.v[index] = new Point(vertex);
   }

   public double getArea()
   {
      int i = 0;
      double a = 0.0;

      for (i = 0; i < this.v.length - 1; i++)
      {
         a += (this.v[i].x * this.v[i + 1].y -
               this.v[i].y * this.v[i + 1].x);
      }

      a += (this.v[i].x * this.v[0].y -
            this.v[i].y * this.v[0].x);

      a /= 2.0;

      return (a > 0) ? a : -a;
   }

   public Point getPosition()
   {
      return new Point(this.v[0]);
   }

   public void setPosition(Point position)
   {
      int xOff = position.x - v[0].x;
      int yOff = position.y - v[0].y;

      for (int i = 0; i < this.v.length; i++)
      {
         this.v[i] = new Point(this.v[i].x + xOff, this.v[i].y + yOff);
      }
   }

   public void move(Point delta)
   {
      for (int i = 0; i < this.v.length; i++)
      {
         this.v[i].setLocation(this.v[i].x + delta.x,
                               this.v[i].y + delta.y);
      }
   }

   public boolean equals(Object other)
   {
      if (!super.equals(other) ||
          this.v.length != ((ConvexPolygon)other).v.length)
      {
         return false;
      }

      for (int i = 0; i < this.v.length; i++)
      {
         if (!this.v[i].equals(((ConvexPolygon)other).v[i]))
         {
            return false;
         }
      }

      return true;
   }
}
