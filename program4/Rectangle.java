/**
 * Rectangle class: extends ConvexPolygon
 *
 * @author Christopher Patton
 * @version Program 4
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Point;
import java.awt.Color;

public class Rectangle extends ConvexPolygon
{
   public Rectangle(int width, int height, Point position, Color color,
                    boolean filled)
   {
      super(new Point[]
            {
               position,
               new Point(position.x + width, position.y),
               new Point(position.x + width, position.y + height),
               new Point(position.x        , position.y + height)
            },
            color, filled);
   }

   public int getWidth()
   {
      return (getVertex(1).x - getVertex(0).x);
   }

   public void setWidth(int width)
   {
      setVertex(1, new Point(getVertex(0).x + width, getVertex(0).y));
      setVertex(2, new Point(getVertex(3).x + width, getVertex(3).y));
   }

   public int getHeight()
   {
      return (getVertex(3).y - getVertex(0).y);
   }

   public void setHeight(int height)
   {
      setVertex(2, new Point(getVertex(1).x, getVertex(1).y + height));
      setVertex(3, new Point(getVertex(0).x, getVertex(0).y + height));
   }
}
