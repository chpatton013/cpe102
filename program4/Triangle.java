/**
 * Triangle class: extends ConvexPolygon
 *
 * @author Christopher Patton
 * @version Program 4
 * @version CPE102-09
 * @version Spring 2011
 */

import java.awt.Point;
import java.awt.Color;

public class Triangle extends ConvexPolygon
{
   public Triangle(Point a, Point b, Point c, Color color, boolean filled)
   {
      super(new Point[] {a, b, c}, color, filled);
   }

   public Point getVertexA()
   {
      return getVertex(0);
   }

   public void setVertexA(Point point)
   {
      setVertex(0, point);
   }

   public Point getVertexB()
   {
      return getVertex(1);
   }

   public void setVertexB(Point point)
   {
      setVertex(1, point);
   }

   public Point getVertexC()
   {
      return getVertex(2);
   }

   public void setVertexC(Point point)
   {
      setVertex(2, point);
   }

}
