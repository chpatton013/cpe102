import java.io.*;

public class BinIO
{
   public static void main (String[] args)
    throws FileNotFoundException, IOException
   {
      DataInputStream in = new DataInputStream(
       new FileInputStream(new File("./file.bin")));

      DataOutputStream out = new DataOutputStream(
       new FileOutputStream(new File("./out.txt")));

      while (true)
      {
         try
         {
            int n = in.readInt();

            out.writeInt(n);

            for (int i = 0; i < n; i++)
               out.writeDouble(in.readDouble());
         }
         catch (EOFException e)
         {
            break;
         }
      }

      in.close();
   }  
}
