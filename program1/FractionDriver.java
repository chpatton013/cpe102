public class FractionDriver
{
   public static void main(String[] args)
   {
      Fraction f1 = new Fraction(1, 2);
      Fraction f2 = new Fraction(6, 8);
      Fraction f3 = new Fraction(4, 1);
      Fraction f4 = new Fraction(6, 3);

      System.out.println("f1: 1/2, f2: 6/8, f3: 4/1, f4: 6/3");
      System.out.println();

      System.out.println("f1: " + f1.getNumerator() + " / " + f1.getDenominator());
      System.out.println("f2: " + f2.getNumerator() + " / " + f2.getDenominator());
      System.out.println("f3: " + f3.getNumerator() + " / " + f3.getDenominator());
      System.out.println("f4: " + f4.getNumerator() + " / " + f4.getDenominator());
      System.out.println();

      System.out.println("f1: " + f1.toString());
      System.out.println("f2: " + f2.toString());
      System.out.println("f3: " + f3.toString());
      System.out.println("f4: " + f4.toString());
      System.out.println();

      System.out.println("f1: " + f1.value());
      System.out.println("f2: " + f2.value());
      System.out.println("f3: " + f3.value());
      System.out.println("f4: " + f4.value());
      System.out.println();

      System.out.println("f1 == f2: " + f1.equals(f2));
      System.out.println("f3 == f4: " + f3.equals(f4));
      System.out.println("f1 == f1: " + f1.equals(f1));
      System.out.println();

      System.out.println("f1 + f2: " + f1.add(f2));
      System.out.println("f3 + f4: " + f3.add(f4));
      System.out.println("f1 + f2 + f3 + f4: " + f1.add(f2.add(f3.add(f4))));
      System.out.println();

      System.out.println("f1 - f2: " + f1.sub(f2));
      System.out.println("f3 - f4: " + f3.sub(f4));
      System.out.println("f1 - f2 - f3 - f4: " + f1.sub(f2.sub(f3.sub(f4))));
      System.out.println();

      System.out.println("f1 * f2: " + f1.mul(f2));
      System.out.println("f3 * f4: " + f3.mul(f4));
      System.out.println("f1 * f2 * f3 * f4: " + f1.mul(f2.mul(f3.mul(f4))));
      System.out.println();

      System.out.println("f1 / f2: " + f1.div(f2));
      System.out.println("f3 / f4: " + f3.div(f4));
      System.out.println("f1 / f2 / f3 / f4: " + f1.div(f2.div(f3.div(f4))));
      System.out.println();

      Fraction f5 = new Fraction();
      System.out.println(f5.getNumerator());
      System.out.println(f5.getDenominator());
      System.out.println(f5.toString());
      System.out.println(f5.value());
      System.out.println(f5.add(f1));
      System.out.println(f5.mul(f1));
      System.out.println();

      Fraction f6 = new Fraction(0, 0);
   }
}
