/**
 * This program has the required constructors and methods to represent
 * and manipulate the fractional data type Fraction
 *
 * @author Christopher Patton
 * @version Program 1
 */

public class Fraction
{
   private int n, d = 1;

   public Fraction()
   {
   }

   public Fraction(int numerator)
   {
      n = numerator;
   }

   public Fraction(int numerator, int denominator)
   {
      if (denominator <= 0)
      {
         throw new IllegalArgumentException();
      }

      n = numerator;
      d = denominator;

      int g = (n > d) ? gcf(n, d) : gcf(d, n);

      if (g != 0 && g > 0)
      {
         n /= g;
         d /= g;
      }

      if (g != 0 && g < 0)
      {
         n /= -g;
         d /= -g;
      }
   }

   private int gcf(int a, int b)
   {
      if (a == 0)
      {
         return b;
      }

      else if (b == 0)
      {
         return a;
      }

      int r = a % b;

      if (r == 0)
      {
        return b;
      }

      else if (r == 1)
      {
         return 0;
      }

      else
      {
         return gcf(b, r);
      }
   }

   public int getNumerator()
   {
      return n;
   }

   public int getDenominator()
   {
      return d;
   }

   public java.lang.String toString()
   {
      if (d == 1)
      {
         return "" + n;
      }

      else
      {
         return n + "/" + d;
      }
   }

   public double value()
   {
      return n/(double)d;
   }

   public boolean equals(Fraction other)
   {
      if (this.n == other.n && this.d == other.d)
      {
         return true;
      }

      else
      {
         return false;
      }
   }

   public Fraction add(Fraction other)
   {
      return new Fraction(this.n * other.d + other.n * this.d,
                          this.d * other.d);
   }

   public Fraction sub(Fraction other)
   {
      return this.add(new Fraction(-other.n, other.d));
   }

   public Fraction mul(Fraction other)
   {
      return new Fraction(this.n * other.n, this.d * other.d);
   }

   public Fraction div(Fraction other)
   {
        return this.mul(new Fraction(other.d, other.n));
   }
}
