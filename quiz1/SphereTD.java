import java.awt.Color;

public class SphereTD
{
   public static void main(String[] args)
   {
      Sphere defaultSphere = new Sphere();
      Sphere radiusSphere = new Sphere(3.0);
      Sphere fullSphere = new Sphere(5.0, Color.blue);

      // getRadius
      if ((int)defaultSphere.getRadius() != 1 &&
          (int)radiusSphere.getRadius() != 3 &&
          (int)fullSphere.getRadius() != 5)
         System.out.println("getRadius Failed");

      // getDiameter
      if ((int)defaultSphere.getDiameter() != 2 &&
          (int)radiusSphere.getDiameter() != 6 &&
          (int)fullSphere.getDiameter() != 10)
         System.out.println("getDiam Failed");

      // getColor
      if (defaultSphere.getColor() != Color.black &&
          radiusSphere.getColor() != Color.black &&
          fullSphere.getColor() != Color.blue)
         System.out.println("getColor Failed");

      // getSA
      if ((int)fullSphere.getSurfaceArea() != (int)(4 * Math.PI * 25))
         System.out.println("getSA Failed");

      // getV
      if ((int)fullSphere.getVolume() != (int)(4 * Math.PI * 125 / 3))
         System.out.println("getV Failed");

      System.out.println(fullSphere.toString());
   }
}
