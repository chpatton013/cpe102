/**
 * @author Christopher Patton
 * @version Lab Quiz 1
 */

import java.awt.Color;

public class Sphere
{
   private double r = 1.0;
   private Color c = Color.black;

   public Sphere()
   {
   }

   public Sphere(double radius)
   {
      r = radius;
   }

   public Sphere(double radius, Color color)
   {
      r = radius;
      c = color;
   }

   public double getRadius()
   {
      return r;
   }

   public double getDiameter()
   {
      return r * 2;
   }

   public Color getColor()
   {
      return c;
   }

   public double getSurfaceArea()
   {
      return 4 * Math.PI * r * r;
   }

   public double getVolume()
   {
      return 4 * Math.PI * r * r * r / 3;
   }

   public String toString()
   {
      return "The Sphere is immutable!";
   }
}