/**
 * Lunar Lander Simulator.
 *
 * @author Christopher Patton
 * @version Program 7
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.text.*;

public class Moonlander extends JFrame
{
   private final static double G = -1.62;
   private final static String title =
    "<html><pre><center>" +
    "<font size=5>Lunar Lander Simulator</font><br />" +
    "<font size=2>Created by Christopher Patton</font>" +
    "</center></pre></html>";

   private int time, fuel = 500, rate;
   private double alt = 1300.00, vel, accel = G;
   private String status = "Separated from Orbiter - Controlled Flight";

   private JLabel titleLabel = new JLabel(title, JLabel.CENTER);
   private JLabel statusLabel = new JLabel(status, JLabel.CENTER);

   private JLabel altLabel =
    new JLabel("Altitude: " + alt + " m", JLabel.CENTER);
   private JLabel velLabel =
    new JLabel("Velocity: " + vel +" m/s", JLabel.CENTER);
   private JLabel accelLabel =
    new JLabel("Acceleration: " + accel +" m/s/s", JLabel.CENTER);
   private JLabel fuelLabel = new JLabel("Fuel: " + fuel +" L", JLabel.CENTER);

   private JLabel rateLabel1 = new JLabel("Fuel Rate: ");
   private JSlider rateSlider = new JSlider(0, 9, 0);
   private JLabel rateLabel2 = new JLabel(rate + " L/s");

   private JLabel timeLabel = new JLabel("Elapsed Time: " + time + " s");
   private JButton updateButton = new JButton("UPDATE");
   private JButton resetButton = new JButton("RESET");
   private JButton quitButton = new JButton("QUIT");

   private JPanel topPanel = new JPanel();
   private JPanel centerPanel = new JPanel();
   private JPanel bottomPanel = new JPanel();

   private JPanel fuelControl = new JPanel();
   private JPanel timeControl = new JPanel();

   private JPanel contentPane = (JPanel)this.getContentPane();


   private class RateListener implements ChangeListener
   {
      public void stateChanged(ChangeEvent event)
      {
         rate = rateSlider.getValue();
         update();
      }
   }

   private class UpdateListener implements ActionListener
   {
      public void actionPerformed(ActionEvent event)
      {
         rate = (rate < fuel) ? rate : fuel;

         if (alt > 0)
         {
            time++;

            accel = G * (1 - (rate / 5.0));
            alt += vel + (accel / 2);
            vel += accel;
            fuel -= rate;

            if (alt <= 0)
            {
               alt = 0;

               if (vel > -5)
               {
                  status = "The Eagle has landed!";
               }
               else if (vel > -10)
               {
                  status = "<html><pre><center>" +
                           "Serious damage to LM - One way ticket - <br />" +
                           "Enjoy your oxygen while it lasts!" +
                           "</center></pre></html>";
               }
               else
               {
                  status = "<html><pre><center>" +
                           "Ouch, that hurt! I guess you won't be <br />" +
                           "making one small step for man..." +
                           "</center></pre></html>";
               }
            }

            if (fuel == 0)
            {
               accel = G;
               outOfFuel();
            }

            update();
         }
      }
   }

   private class ResetListener implements ActionListener
   {
      public void actionPerformed(ActionEvent event)
      {
         time = 0;
         fuel = 500;
         rate = 0;
         alt = 1300.00;
         vel = 0;
         accel = G;
         status = "Separated from Orbiter - Controlled Flight";

         rateSlider.setValue(0);

         update();
      }
   }

   private class QuitListener implements ActionListener
   {
      public void actionPerformed(ActionEvent event)
      {
         System.exit(0);
      }
   }

   private ChangeListener rateListener = new RateListener();
   private ActionListener updateListener = new UpdateListener();
   private ActionListener resetListener = new ResetListener();
   private ActionListener quitListener = new QuitListener();

   private DecimalFormat dFmt = new DecimalFormat("#0.00");
   private DecimalFormat tFmt = new DecimalFormat("#000");

   public Moonlander()
   {
      titleLabel.setFont(new Font("Courier New", Font.BOLD, 12));
      statusLabel.setFont(new Font("Courier New", Font.BOLD, 12));
      altLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
      velLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
      accelLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
      fuelLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
      rateLabel1.setFont(new Font("Courier New", Font.PLAIN, 12));
      rateLabel2.setFont(new Font("Courier New", Font.PLAIN, 12));
      timeLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
      updateButton.setFont(new Font("Courier New", Font.BOLD, 12));
      resetButton.setFont(new Font("Courier New", Font.BOLD, 12));
      quitButton.setFont(new Font("Courier New", Font.BOLD, 12));

      update();

      topPanel.setLayout(new GridLayout(2, 1));
      topPanel.add(titleLabel);
      topPanel.add(statusLabel);

      centerPanel.setLayout(new GridLayout(2, 2));
      centerPanel.add(altLabel);
      centerPanel.add(velLabel);
      centerPanel.add(accelLabel);
      centerPanel.add(fuelLabel);

      fuelControl.setLayout(new FlowLayout());
      fuelControl.add(rateLabel1);
      fuelControl.add(rateSlider);
      fuelControl.add(rateLabel2);

      timeControl.setLayout(new FlowLayout());
      timeControl.add(timeLabel);
      timeControl.add(updateButton);
      timeControl.add(resetButton);
      timeControl.add(quitButton);

      bottomPanel.setLayout(new GridLayout(2, 1));
      bottomPanel.add(fuelControl);
      bottomPanel.add(timeControl);

      contentPane.add(topPanel, BorderLayout.NORTH);
      contentPane.add(centerPanel, BorderLayout.CENTER);
      contentPane.add(bottomPanel, BorderLayout.SOUTH);

      rateSlider.addChangeListener(rateListener);
      updateButton.addActionListener(updateListener);
      resetButton.addActionListener(resetListener);
      quitButton.addActionListener(quitListener);

      pack();
   }

   private void outOfFuel()
   {
      while (alt > 0)
      {
         update();

         time++;

         alt += vel + (accel / 2);
         vel += accel;

         if (alt <= 0)
         {
            alt = 0;

            if (vel > -5)
            {
               status = "The Eagle has landed!";
            }
            else if (vel > -10)
            {
               status = "<html><pre><center>" +
                        "Serious damage to LM - One way ticket - <br />" +
                        "Enjoy your oxygen while it lasts!" +
                        "</center></pre></html>";
            }
            else
            {
               status = "<html><pre><center>" +
                        "Ouch, that hurt! I guess you won't be <br />" +
                        "making one small step for man..." +
                        "<center></pre></html>";
            }
         }
      }
   }

   private void update()
   {
      statusLabel.setText(status);
      altLabel.setText("Altitude: " + dFmt.format(alt) + " m");
      velLabel.setText("Velocity: " + dFmt.format(vel) + " m/s");
      accelLabel.setText("Acceleration: " + dFmt.format(accel) + " m/s/s");
      fuelLabel.setText("Fuel: " + fuel + " L");
      rateLabel2.setText(rate + " L/s");
      timeLabel.setText("Elapsed Time: " + tFmt.format(time) + " s");
   }

   public static void main(String[] args)
   {
      Moonlander frame = new Moonlander();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setVisible(true);
   }
}
