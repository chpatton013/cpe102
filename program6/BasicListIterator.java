/**
 * Basic List Iterator Class
 *
 * @author Christopher Patton
 * @version Program 6
 */

import java.util.Iterator;

public interface BasicListIterator<E> extends Iterator<E>
{
   boolean hasPrevious();
   E previous();
}
