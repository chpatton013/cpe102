/**
 * Basic Linked List Class
 *
 * @author Christopher Patton
 * @version Program 6
 */

import java.util.Iterator;
import java.util.NoSuchElementException;

public class BasicLinkedList<E> implements BasicList<E>, Iterable<E>
{
   private Node head = new Node();;
   private int lSize;

   public BasicLinkedList()
   {
      head.prev = head;
      head.next = head;
   }

   private class Node
   {
      public E data;
      public Node next, prev;
   }

   private class It implements BasicListIterator<E>
   {
      private Node cursor = head;

      public void remove()
      {
         throw new UnsupportedOperationException();
      }

      public boolean hasNext()
      {
         return cursor.next != head;
      }

      public E next()
      {
         if (!hasNext())
         {
            throw new NoSuchElementException();
         }

         cursor = cursor.next;
         return cursor.data;
      }

      public boolean hasPrevious()
      {
         return cursor != head;
      }

      public E previous()
      {
         if (!hasPrevious())
         {
            throw new NoSuchElementException();
         }

         cursor = cursor.prev;
         return cursor.next.data;
      }
   }

   public void add(E element)
   {
      Node n = new Node();

      n.data = element;
      n.prev = head.prev;
      n.next = head;

      head.prev.next = n;
      head.prev = n;

      lSize++;
   }

   public void add(int index, E element)
   {
      if (index > lSize || index < 0)
      {
         throw new IndexOutOfBoundsException();
      }

      Node n = new Node();

      n.data = element;
      n.next = (index == lSize) ? head : getNode(index);
      n.prev = n.next.prev;

      n.next.prev = n;
      n.prev.next = n;

      lSize++;
   }

   public BasicListIterator<E> basicListIterator()
   {
      return new It();
   }

   public void clear()
   {
      head.next = head;
      head.prev = head;

      lSize = 0;
   }

   public boolean contains(E element)
   {
      try
      {
         indexOf(element);
         return true;
      }

      catch (NoSuchElementException e)
      {
         return false;
      }
   }

   public E get(int index)
   {
      return getNode(index).data;
   }

   public int indexOf(E element)
   {
      if (lSize == 0)
      {
         throw new NoSuchElementException();
      }

      Node temp = head.next;
      int index = 0;

      do {
         if (temp.data.equals(element))
         {
            return index;
         }

         temp = temp.next;
         index++;
      } while (temp != head);

      throw new NoSuchElementException();
   }

   public Iterator<E> iterator()
   {
      return new It();
   }

   public E remove(int index)
   {
      Node ref = getNode(index);
      E data = ref.data;

      ref.prev.next = ref.next;
      ref.next.prev = ref.prev;

      lSize--;

      return data;
   }

   public E set(int index, E element)
   {
      Node ref = getNode(index);
      E data = ref.data;

      ref.data = element;

      return data;
   }

   public int size()
   {
      return lSize;
   }

   private Node getNode(int index)
   {
      if (index >= lSize || index < 0)
      {
         throw new IndexOutOfBoundsException();
      }

      Node temp = head;

      for (int i = 0; i < index; i++)
      {
         temp = temp.next;
      }

      return temp.next;
   }
}
