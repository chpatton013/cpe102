public class Student extends Person
{
   private double gpa;

   public Student (double GPA, String name, int legs)
   {
      super(name, legs);
      gpa = GPA;
   }

   public double getGPA()
   {
      return gpa;
   }

   public boolean equals(Object o)
   {
      return (o != null && this.getClass() == o.getClass() &&
              this.getLegs() == ((Student)o).getLegs() &&
              this.getName().equals(((Student)o).getName()) &&
              this.gpa == ((Student)o).gpa);
   }

   public String toString()
   {
      return (super.toString() + "and a Student Object with a " + 
              this.gpa + "gpa");
   }
}
