public class Animal
{
   private int l = 0;

   public Animal (int legs)
   {
      l = legs;
   }

   public int getLegs()
   {
      return this.l;
   }

   public boolean equals (Object o)
   {
      return (o != null && this.getClass() == o.getClass() &&
              this.l == ((Animal)o).l);
   }

   public String toString()
   {
      return ("I am an Animal object with " + this.l + "legs");
   }
}
