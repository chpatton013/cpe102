public class Person extends Animal
{
   private String n;

   public Person (String name, int legs)
   {
      super(legs);
      n = name;
   }

   public String getName()
   {
      return this.n;
   }

   public boolean equals (Object o)
   {
      return (o != null && this.getClass() == o.getClass() &&
              this.getLegs() == ((Person)o).getLegs() &&
              this.n.equals(((Person)o).n));
   }

   public String toString()
   {
      return (super.toString() + "and a Person object whose name is " + 
              this.n);
   }
}
