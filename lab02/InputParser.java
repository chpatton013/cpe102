import java.util.Scanner;

public class InputParser
{
   public static void main(String args[])
   {
      int numInts = 0, numDbls = 0, numStrs = 0;
      double sum = 0;
      String str = new String();

      int[] ints;
      double[] dbls;
      String[] strs;

      ints = new int[3];
      dbls = new double[3];
      strs = new String[3];

      System.out.print("Input three data values.");
      Scanner sc = new Scanner(System.in);

      for (int i = 0; i < 3; i++)
      {
         if (sc.hasNextInt())
         {
            ints[numInts] = sc.nextInt();
            numInts++;
         }

         else if (sc.hasNextDouble())
         {
            dbls[numDbls] = sc.nextDouble();
            numDbls++;
         }

         else if (sc.hasNext())
         {
            strs[numStrs] = sc.next();
            numStrs++;
         }
      }

      for (int i = 0; i < numInts; i++)
      {
         sum += ints[i];
      }

      for (int i = 0; i < numDbls; i++)
      {
         sum += dbls[i];
      }

      for (int i = 0; i < numStrs; i++)
      {
         str += strs[i];
      }

      System.out.println("Ints: " + numInts + ", Doubles: " + numDbls +
       ", Strings: " + numStrs);

      if ((numInts + numDbls) > 0)
      {
         System.out.println("Sum of ints and doubles: " + sum);
      }

      if (numStrs > 0)
      {      
         System.out.println("Concatenation of strings: " + str);
      }
   }
}
