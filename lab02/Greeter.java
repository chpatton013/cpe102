public class Greeter
{
   private String n;

   public Greeter(String name)
   {
      n = name;
   }

   public String greet()
   {
      return "Hello " + n;
   }
}
