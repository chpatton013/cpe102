public class BadTest
{
   public static void main(String[] args)
   {
      Bad bad = new Bad();

      try {
         bad.nullPtr();
         bad.classExcep();
         bad.ndxOutOfBounds();
      }

      catch (NullPointerException ref) {}
      catch (ClassCastException ref) {}
      catch(ArrayIndexOutOfBoundsException ref) {}

      finally {
         System.out.println("Finally!");
      }
   }
}
