public class Bad
{
   public Bad() {}

   public void nullPtr()
   {
      throw new NullPointerException();
   }

   public void classExcep()
   {
      throw new ClassCastException();
   }

   public void ndxOutOfBounds()
   {
      throw new IndexOutOfBoundsException();
   }

   public void choose(int i)
   {
      if (i == 1) nullPtr();
      if (i == 2) classExcep();
      if (i == 3) ndxOutOfBounds();
   }
}
