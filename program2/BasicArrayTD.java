public class BasicArrayTD
{
   public static void main(String[] args)
   {
      BasicArrayList dList = new BasicArrayList();
      BasicArrayList list = new BasicArrayList(20);

      if (dList.capacity() != 10)
      {
         System.out.println("dList.capacity() == " + dList.capacity());
      }

      if (list.capacity() != 20)
      {
         System.out.println("list.capacity() == " + list.capacity());
      }

      for (int i = 0; i < 9; i++)
      {
         dList.add("Hello");
         list.add("Hello");

         if (dList.get(i) != "Hello")
         {
            System.out.println("dList.get(" + i + ") == " + dList.get(i));
         }

         if (list.get(i) != "Hello")
         {
            System.out.println("list.get(" + i + ") == " + list.get(i));
         }
      }

      dList.add(1, "World!");
      list.add(1, "World!");

      if (dList.get(1) != "World!")
      {
         System.out.println("dList.get(1) == " + dList.get(1));
      }

      if (list.get(1) != "World!")
      {
         System.out.println("list.get(1) == " + list.get(1));
      }

      if (dList.contains("Hello") == false)
      {
         System.out.println("dList.contains('Hello') == " +
                             dList.contains("Hello"));
      }

      if (dList.contains("World!") == false)
      {
         System.out.println("dList.contains('World!') == " +
                             dList.contains("World!"));
      }

      if (list.contains("Hello") == false)
      {
         System.out.println("list.contains('Hello') == " +
                             list.contains("Hello"));
      }

      if (list.contains("World!") == false)
      {
         System.out.println("list.contains('World!') == " +
                             list.contains("World!"));
      }

      dList.clear();

      if (dList.contains("Hello") || dList.contains("World!"))
      {
         System.out.println("dList is not cleared");
      }

      if (dList.size() != 0)
      {
         System.out.println("dList.size() == " + dList.size());
      }

      if (list.size() != 10)
      {
         System.out.println("list.size() == " + list.size());
      }

      if (list.indexOf("World!") != 1)
      {
         System.out.println("list.indexOf('World!') == " +
                             list.indexOf("World!"));
      }

      if (list.remove(1) != "World!")
      {
         System.out.println("list.remove(1) fail");
      }

      if (list.contains("World!"))
      {
         System.out.println("list.contains('World!') == " + 
                             list.contains("World!"));
      }

      if (list.size() != 9)
      {
         System.out.println("list.size() == " + list.size());
      }

      if (list.set(1, "World!") != "Hello")
      {
         System.out.println("list.set(1, 'World!') fail");
      }

      list.trimToSize();

      if (list.capacity() != 10)
      {
         System.out.println("list.capacity() == " + list.capacity());
      }

      for (int i = 9; i < 15; i++)
      {
         list.add(i, "Hello"); //  exception. unable to reallocate

         if (list.get(i) != "Hello")
         {
            System.out.println("list.get(" + i + ") == " + list.get(i));
         }
      }

      list.add("World!");

      if (list.get(15) != "World!")
      {
         System.out.println("list.get(15) == " + list.get(15));
      }

      if (list.capacity() != 20)
      {
         System.out.println("list.capacity() == " + list.capacity());
      }

      if (list.size() != 16)
      {
         System.out.println("list.size() == " + list.size());
      }

      list.trimToSize();

      if (list.capacity() != 16)
      {
         System.out.println("list.capacity() == " + list.capacity());
      }
   }
}