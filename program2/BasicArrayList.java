/**
 * This class contains the appropriate constructors and methods to
 * create and maintain a basic list of Objects.
 *
 * @author Christopher Patton
 * @version Program 2
 */

public class BasicArrayList implements BasicList<Object>
{
   public static final int DEFAULT_CAPACITY = 10;

   private Object[] obj = new Object[DEFAULT_CAPACITY];
   private int lSize;

   public BasicArrayList()
   {
   }

   public BasicArrayList(int capacity)
   {
      int cap = (capacity > DEFAULT_CAPACITY) ? capacity : DEFAULT_CAPACITY;
      obj = new Object[cap];
   }

   private void insert(int index, java.lang.Object o, java.lang.Object[] temp)
   {
      for (int i = lSize; i > index; i--)
      {
         temp[i] = this.obj[i - 1];
      }

      temp[index] = o;

      for (int i = 0; i < index; i++)
      {
         temp[i] = this.obj[i];
      }
   }

   public void add(int index, java.lang.Object o)
   {
      if (index < 0 || index > lSize)
      {
         throw new java.lang.IndexOutOfBoundsException();
      }

      if (lSize == this.obj.length)
      {
         Object[] temp = new Object[this.obj.length * 2];

         insert(index, o, temp);

         this.obj = temp;
      }

      else
      {
         insert(index, o, this.obj);
      }

      lSize++;
   }

   public void add(java.lang.Object o)
   {
      add(lSize, o);
   }

   public int capacity()
   {
      return this.obj.length;
   }

   public void clear()
   {
      obj = new Object[DEFAULT_CAPACITY];
      lSize = 0;
   }

   public boolean contains(java.lang.Object o)
   {
      for (int i = 0; i < lSize; i++)
      {
         if (obj[i].equals(o))
         {
            return true;
         }
      }

      return false;
   }

   public java.lang.Object get(int index)
   {
      if (index < 0 || index >= lSize)
      {
         throw new java.lang.IndexOutOfBoundsException();
      }

      return this.obj[index];
   }

   public int indexOf(java.lang.Object o)
   {
      for (int i = 0; i < lSize; i++)
      {
         if (this.obj[i].equals(o))
         {
            return i;
         }
      }

      throw new java.util.NoSuchElementException();
   }

   public java.lang.Object set(int index, java.lang.Object o)
   {
      if (index < 0 || index >= lSize)
      {
         throw new java.lang.IndexOutOfBoundsException();
      }

      Object temp = this.obj[index];
      this.obj[index] = o;

      return temp;
   }

   public java.lang.Object remove(int index)
   {
      if (index < 0 || index >= lSize)
      {
         throw new java.lang.IndexOutOfBoundsException();
      }

      Object temp = this.obj[index];

      lSize--;

      for (int i = index; i < lSize; i++)
      {
         this.obj[i] = this.obj[i + 1];
      }

      return temp;
   }

   public int size()
   {
      return lSize;
   }

   public void trimToSize()
   {
      Object[] temp = (lSize > DEFAULT_CAPACITY) ? new Object[lSize] :
                       new Object[DEFAULT_CAPACITY];

      for (int i = 0; i < lSize; i++)
      {
         temp[i] = this.obj[i];
      }

      this.obj = temp;
   }

   public java.lang.Object[] unusualMethodForTestDriverOnly()
   {
      return this.obj;
   }
}
