import java.util.Scanner;

public class GenericTD
{
   public static void main(String[] args)
   {
      Scanner sc = new Scanner(System.in);
      GenericArrayList list = new GenericArrayList();
      String str = new String();

      while (true)
      {
         if (sc.hasNextInt())
         {
            list.add(sc.nextInt());
         }

         else if (sc.hasNextDouble())
         {
            list.add(sc.nextDouble());
         }

         else if (sc.hasNextBoolean())
         {
            list.add(sc.nextBoolean());
         }

         else
         {
            str = sc.next();

            if (str.equals("quit"))
            {
               break;
            }

            else
            {
               list.add(str);
            }
         }
      }

      System.out.println("Minimum Int: " + list.minimumInt());
      System.out.println("Average Double " + list.averageDouble());
      System.out.println("Number of Trues " + list.numberOfTrues());
      System.out.println("Number of Strings " + list.numberOfStrings());
   }
}
