import java.util.ArrayList;


public class GenericArrayList
{
   private ArrayList<Integer> myIntList = new ArrayList<Integer>();
   private ArrayList<Double> myDblList = new ArrayList<Double>();
   private ArrayList<Boolean> myBoolList = new ArrayList<Boolean>();
   private ArrayList<String> myStrList = new ArrayList<String>();

   public GenericArrayList()
   {
   }

   public void add(int myInt)
   {
      myIntList.add(new Integer(myInt));
   }

   public void add(double myDbl)
   {
      myDblList.add(new Double(myDbl));
   }

   public void add(boolean myBool)
   {
      myBoolList.add(new Boolean(myBool));
   }

   public void add(String myStr)
   {
      myStrList.add(myStr);
   }

   public int minimumInt()
   {
      if (myIntList.size() == 0)
      {
         return 0;
      }

      int min = myIntList.get(0);

      for (int i = 1; i < myIntList.size(); i++)
      {
         if (myIntList.get(i) < min)
         {
            min = myIntList.get(i);
         }
      }

      return min;
   }

   public double averageDouble()
   {
      if (myDblList.size() == 0)
      {
         return 0;
      }

      double sum = 0.0;

      for (int i = 0; i < myDblList.size(); i++)
      {
         sum += myDblList.get(i);
      }

      return sum / myDblList.size();
   }

   public int numberOfTrues()
   {
      int numTrues = 0;

      for (int i = 0; i < myBoolList.size(); i++)
      {
         if (myBoolList.get(i))
         {
            numTrues++;
         }
      }

      return numTrues;
   }

   public int numberOfStrings()
   {
      return myStrList.size();
   }
}
