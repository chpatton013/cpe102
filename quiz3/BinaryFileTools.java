import java.io.*;
import java.util.*;

public class BinaryFileTools
{
   public static ArrayList<Object> read(String name)
    throws FileNotFoundException, IOException, FileFormatException
   {
      ArrayList<Object> list = new ArrayList<Object>();
      DataInputStream in = new DataInputStream(new FileInputStream(name));
      byte mode;

      while (true)
      {
         try
         {
            mode = in.readByte();

            if (mode == 1)
            {
               list.add(new Boolean(in.readBoolean()));
            }
            else if (mode == 2)
            {
               list.add(new Float(in.readFloat()));
            }
            else if (mode == 3)
            {
               list.add(new Long(in.readLong()));
            }
            else
            {
               throw new FileFormatException();
            }
         }
         catch(EOFException e)
         {
            break;
         }
      }

      in.close();

      return list;
   }

  
   public static void write(String name, ArrayList<Object> list)
    throws FileNotFoundException, IOException, FileFormatException
   {
      DataOutputStream out = new DataOutputStream(new FileOutputStream(name));
      int mode, i = 0;

      while (true)
      {
         try
         {
            if (list.get(i) instanceof Boolean)
            {
               out.writeBoolean((Boolean)list.get(i));
            }
            else if (list.get(i) instanceof Float)
            {
               out.writeFloat((Float)list.get(i));
            }
            else if (list.get(i) instanceof Long)
            {
               out.writeLong((Long)list.get(i));
            }
            else
            {
               throw new IllegalArgumentException();
            }
         }
         catch(IndexOutOfBoundsException e)
         {
            break;
         }
         finally
         {
            i++;
         }
      }

      out.close();
   }
   
}
